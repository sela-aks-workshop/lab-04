# Kubernetes Workshop
Lab 04: Creating a Load Balancer Service

---

## Application Architecture

<img alt="voting-app-architecture" src="voting-app-architecture.png"  width="60%" height="60%">


## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get deployments
```

 - List existent pods
```
kubectl get pods
```

### Get the application resources

 - Clone the lab repository
```
git clone https://gitlab.com/sela-aks-workshop/lab-04.git ~/lab-04
cd ~/lab-04
```

 - List deployments resources
```
ls -l ~/lab-04/k8s-deployments/
```

### Create the kubernetes deployments

 - Create deployment resources
```
kubectl apply -f ~/lab-04/k8s-deployments/
```

 - List existent deployments
```
kubectl get deployments
```

 - List existent pods
```
kubectl get pods
```

 - The worker pod will failed because there is still no connection between the pods (services)
```
NAME                      READY   STATUS             RESTARTS   AGE
db-5696969744-sjrsj       1/1     Running            0          6m
redis-556f489c7c-7xjgx    1/1     Running            0          6m
result-84868df65f-dmjvh   1/1     Running            0          6m
vote-6df549f448-58kkk     1/1     Running            0          6m
worker-777dc9c586-bb65h   0/1     CrashLoopBackOff   5          6m
```

### Create the kubernetes services

 - List existent services
```
kubectl get services
```

 - The "db" and "redis" services should be accessible only within the cluster so we can configure them using "ClusterIP" services
```
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  type: ClusterIP
  ports:
  - port: 5432
    targetPort: 5432
  selector:
    app: db
```

 - Let's create both services
```
kubectl apply -f ~/lab-04/k8s-services/db-service.yaml
kubectl apply -f ~/lab-04/k8s-services/redis-service.yaml
```

 - The "result" and "vote" services should be accessible from outside the cluster so we can configure them using "LoadBalancer" services
```
apiVersion: v1
kind: Service
metadata:
  name: vote
spec:
  type: LoadBalancer
  ports:
  - name: "vote-service"
    port: 5000
    targetPort: 80
  selector:
    app: vote
```

 - Let's create both services
```
kubectl apply -f ~/lab-04/k8s-services/result-service.yaml
kubectl apply -f ~/lab-04/k8s-services/vote-service.yaml
```

 - List existent services
```
kubectl get services -w
```

 - Wait a bit until the service finalizes to create the Load Balancer and is no longer pending. Then ensure the worker service started to work
```
kubectl get pods
```

### Access the application

 - Access to the vote service and make your vote:
```
http://<vote-service-lb-ip>:5000
```

 - Access to the result service and check the results:
```
http://<result-service-lb-ip>:5001
```

### Scale up the vote service to test the internal load balancing

 - Scale the vote service from "1" to "5" replicas:
```
kubectl edit deployment vote
```

 - List pods related to the vote deployment
```
kubectl get pods -l app=vote
```

 - Browse to the vote service and check the pod that is managing the request under "Processed by container ID"
```
http://<vote-service-lb-ip>:5000
```

 - Refresh the page several times to see how the traffic is redirected to different pods

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```